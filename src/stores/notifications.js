import { writable } from "svelte/store";
import Guid from "../_helpers/Guid";

const store = () => {
	const state = {
        notifications: []
	};

	const { subscribe, set, update } = writable(state);

	const methods = {
		async pushNotification({ title, content, type, action = null, duration = 5000, actionLabel = "", actionFn = () => {} }) {
            const notification = {
                id: Guid.NewGuid(),
                title,
                content,
                type,
                action,
                duration,
                actionLabel,
                actionFn: () => {
                    actionFn();
                    methods.removeNotification(notification.id);
                }
            }

            update((state) => {
                state.notifications = [...state.notifications, notification];
                return state;
            })

            if(state.notifications.length && state.notifications[0].id === notification.id) {
                methods.startNotification(notification);
            }
        },
        startNotification(notification) {
            if(notification.duration !== Infinity) {
                setTimeout(function () {
                    methods.removeNotification(notification.id);
                }, notification.duration);
            }
        },
        removeNotification(id) {
            update((state) => {
                state.notifications = state.notifications.filter((n) => n.id !== id);
                return state;
            });
            if(state.notifications.length) {
                return methods.startNotification(state.notifications[0]);
            }
        }
	};

	return {
		subscribe,
		set,
		update,
		...methods
	};
};

export default store();
